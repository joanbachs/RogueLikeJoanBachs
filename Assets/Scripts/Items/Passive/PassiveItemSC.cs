﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PassiveItem", menuName = "ScriptableObjects/PassiveItem", order = 2)]

public class PassiveItemSC : ItemSC
{
    public boost BoostType;
    public int boostUpgrade;
    public enum boost{
        MaxHealth,
        DashCooldownReduction
    }
}
