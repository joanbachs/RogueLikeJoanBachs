﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponData", menuName = "ScriptableObjects/WeaponData", order = 1)]

public class WeaponSC : ItemSC
{
    public int Damage;
    public int BulletAmount;
    public int MaxBulletCapacity;
    public int BulletsXShoot;

}
