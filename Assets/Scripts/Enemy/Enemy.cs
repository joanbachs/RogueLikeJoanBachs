﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public DataPlayer dp;
    public float life;
    public int dmg;
    public int score;

    private void Start()
    {
        dp = GameManager.Instance.Player.GetComponent<DataPlayer>();
    }
}
