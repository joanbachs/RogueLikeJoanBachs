﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    private DataPlayer dp;
    private int dmg;

    private void Start()
    {
        dp = GameManager.Instance.Player.GetComponent<DataPlayer>();
        dmg = 10;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.Player.GetComponent<PlayerMovement>().canBeDamaged)
            {
                UI.health -= dmg;
                dp.SetDamage(dmg);
                Destroy(gameObject);
            }
        }

        if (collision.gameObject.CompareTag("Deadzone"))
        {
            Destroy(gameObject);
        }

        if (collision.gameObject.name.Equals("Stone"))
        {
            Destroy(gameObject);
        }
    }
}
