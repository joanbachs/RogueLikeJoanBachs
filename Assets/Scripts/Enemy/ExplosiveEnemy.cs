﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveEnemy : Enemy
{
    [SerializeField] private float speed;
    private Transform target;
    private float step;


    // Start is called before the first frame update
    void Start()
    {
        dp = GameManager.Instance.Player.GetComponent<DataPlayer>();
        target = GameObject.Find("Player").GetComponent<Transform>();
        dmg = 20;
        score = 10;
    }

    // Update is called once per frame
    void Update()
    {
        step = speed * Time.deltaTime;
        Vector3 direction = transform.position - target.position;

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

        transform.position = Vector3.MoveTowards(transform.position, target.position, step);
        transform.rotation = Quaternion.AngleAxis(angle + 90, Vector3.forward);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.Player.GetComponent<PlayerMovement>().canBeDamaged)
            {
                dp.SetDamage(dmg);
                Die();
            }
        }

        if (collision.gameObject.CompareTag("Bullet"))
        {
            GameManager.Instance.IncreaseScore(score);
            Die();
        }
    }

    private void Die()
    {
        GameManager.Instance.reduceEnemiesNumber();
        GameManager.Instance.increaseCoins(15);
        Destroy(gameObject);
    }
}
