﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretEnemy : Enemy
{
    [SerializeField] private float rotationSpeed;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private Transform target;
    [SerializeField] private Rigidbody2D bullet;
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 angle;
    private Quaternion targetRotation;
    private Vector3 shootVector;

    private void Start()
    {
        InvokeRepeating("Shoot", 2.0f, 3f);
        target = GameObject.Find("Player").GetComponent<Transform>();
        life = 10;
        dmg = 10;
        score = 7;
    }

    // Update is called once per frame
    void Update()
    {
        //Tracking system
        lastPosition = target.transform.position;

        shootVector = lastPosition - transform.position;
        angle = Quaternion.Euler(0, 0, 180) * shootVector;

        targetRotation = Quaternion.LookRotation(forward: Vector3.forward, upwards: angle);

        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    void Shoot()
    {
        Rigidbody2D instance = Instantiate(bullet, transform.position, targetRotation);
        shootVector.Normalize();
        instance.velocity = shootVector * bulletSpeed;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            if (life <= 0)
            {
                GameManager.Instance.IncreaseScore(score);
                Die();
            }
            else
            {
                life -= collision.gameObject.GetComponent<Bullet>().Dmg;
            }
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (GameManager.Instance.Player.GetComponent<PlayerMovement>().canBeDamaged)
            {
                dp.SetDamage(dmg);
                Die();
            }
        }
    }

    private void Die()
    {
        GameManager.Instance.reduceEnemiesNumber();
        GameManager.Instance.increaseCoins(5);
        Destroy(gameObject);
    }
}
