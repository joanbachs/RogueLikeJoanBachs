﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class HighestScore : MonoBehaviour
{
    [SerializeField] Text highestScoreText;
    private int score;

    void Start()
    {
        score = PlayerPrefs.GetInt("HighestScore");
        highestScoreText.text += score + " pts";
    }
}
