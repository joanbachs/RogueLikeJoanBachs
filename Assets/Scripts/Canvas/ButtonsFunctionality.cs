﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonsFunctionality : MonoBehaviour
{
    public void OnQuitButtonClick()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

    public void OnInitButtonClick()
    {
        SceneManager.LoadScene(1);
    }

    public void OnShopButtonClick()
    {
        SceneManager.LoadScene(3);
    }

    public void OnMenuButtonClick()
    {
        SceneManager.LoadScene(0);
    }

    public void OnHTPButtonClick()
    {
        SceneManager.LoadScene(4);
    }

    public void OnPlayAgainButtonClick()
    {
        SceneManager.LoadScene(1);
    }
}
