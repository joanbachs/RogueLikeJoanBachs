﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthText : MonoBehaviour
{
    [SerializeField] private Text hpText;
    private DataPlayer dp;

    void Start()
    {
        dp = GameManager.Instance.Player.GetComponent<DataPlayer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        hpText.text = dp.CurrentHp.ToString();
    }
}
