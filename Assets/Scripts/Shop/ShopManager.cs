﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    public int[,] shopItems = new int[5, 5];
    [SerializeField] private int coins;
    public Text CoinsTXT;

    void Start()
    {
        coins = PlayerPrefs.GetInt("Coins");
        CoinsTXT.text = "Coins: $" + coins.ToString();

        //ID's
        shopItems[1, 1] = 1;
        shopItems[1, 2] = 2;
        shopItems[1, 3] = 3;
        shopItems[1, 4] = 4;

        //Price
        shopItems[2, 1] = 10;
        shopItems[2, 2] = 20;
        shopItems[2, 3] = 30;
        shopItems[2, 4] = 40;

        //Quantity
        shopItems[3, 1] = 0;
        shopItems[3, 2] = 0;
        shopItems[3, 3] = 0;
        shopItems[3, 4] = 0;
    }

    public void Update()
    {
        coins = GameManager.Instance.Coins;
        CoinsTXT.text = "Coins: $" + coins.ToString();
    }
    public void Buy()
    {
        GameObject ButtonRef = GameObject.FindGameObjectWithTag("Event").GetComponent<EventSystem>().currentSelectedGameObject;

        if (coins >= shopItems[2, ButtonRef.GetComponent<ButtonInfo>().ItemID])
        {
            //coins -= shopItems[2, ButtonRef.GetComponent<ButtonInfo>().ItemID];
            GameManager.Instance.reduceCoins(shopItems[2, ButtonRef.GetComponent<ButtonInfo>().ItemID]);

            shopItems[3, ButtonRef.GetComponent<ButtonInfo>().ItemID]++;

            switch (ButtonRef.GetComponent<ButtonInfo>().ItemID)
            {
                case 1:
                    GameManager.Instance.increasePistolDmg();
                    break;

                case 2:
                    GameManager.Instance.increaseMGDmg();

                    break;

                case 3:
                    GameManager.Instance.increaseSniperDmg();
                    break;

                case 4:
                    GameManager.Instance.increaseSpeed();
                    break;

                default:
                    break;
            }


            //CoinsTXT.text = "Coins: " + coins.ToString();
            ButtonRef.GetComponent<ButtonInfo>().QuantityTxt.text = shopItems[3, ButtonRef.GetComponent<ButtonInfo>().ItemID].ToString();
        }
    }


}
