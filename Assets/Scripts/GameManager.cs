﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    private int _score;
    private int _bestScore;
    private int _coins;
    private int _enemiesOnScene;
    private int _enemiesKilled;
    private float _spawnRate;
    private float _playerSpeed;

    public int Score { get => _score; set => _score = value; }
    public int BestScore { get => _bestScore; set => _bestScore = value; }
    public int Coins { get => _coins; set => _coins = value; }
    public int EnemiesOnScene { get => _enemiesOnScene; set => _enemiesOnScene = value; }
    public int EnemiesKilled { get => _enemiesKilled; set => _enemiesKilled = value; }
    public float SpawnRate { get => _spawnRate; set => _spawnRate = value; }
    public float PlayerSpeed { get => _playerSpeed; set => _playerSpeed = value; }

    public GameObject Player;
    public GameObject UI;
    private RoomManager RM;

    public static GameManager Instance
    {
        get { return _instance; }
    }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;

        Score = 0;
        BestScore = 0;
        EnemiesOnScene = 0;
        Coins = PlayerPrefs.GetInt("Coins");
        if (PlayerPrefs.GetFloat("PlayerSpeed") == 0)
        {
            PlayerSpeed = 1;
        }
        else
        {
            PlayerSpeed = PlayerPrefs.GetFloat("PlayerSpeed");
        }
    }

    private void Update()
    {
        //if (SpawnController.Instance.AllEnemiesSpawned)
        //{
        //    if (EnemiesOnScene <= 0)
        //    {
        //        //if (nextRoomCollider)
        //        //{
        //        //    RM.nextRoom();
        //        //    nextRoomCollider = false;
        //        //}

        //        //loadResultScene();
        //    }
        //}

        //GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        //foreach (GameObject enemy in enemies)
        //    EnemiesOnScene++;

        if (Score > BestScore)
        {
            BestScore = Score;
            PlayerPrefs.SetInt("HighestScore", BestScore);
        }
        PlayerPrefs.SetInt("Coins", Coins);


        if(EnemiesKilled <= 5)
        {
            SpawnRate = 5f;
        }
        else if (EnemiesKilled <= 10){
            SpawnRate = 3.5f;
        }
        else if (EnemiesKilled <= 15)
        {
            SpawnRate = 2f;
        }
        else if (EnemiesKilled <= 20)
        {
            SpawnRate = 0.7f;
        }
        else if (EnemiesKilled > 20)
        {
            SpawnRate = 0.4f;
        }
        else
        {
            SpawnRate = 1f;
        }
    }

    public void IncreaseScore(int score)
    {
        Score += score;
    }

    public void increaseEnemiesNumber()
    {
        EnemiesOnScene++;
    }

    public void reduceEnemiesNumber()
    {
        EnemiesOnScene--;
        EnemiesKilled++;
    }

    public void increaseCoins(int coins)
    {
        Coins +=coins ;
    }
    public void reduceCoins(int coins)
    {
        Coins -= coins;

    }

    public void increaseSpeed()
    {
        PlayerSpeed += 0.5f;
        Debug.Log(PlayerSpeed);
        PlayerPrefs.SetFloat("PlayerSpeed", PlayerSpeed);
    }

    [SerializeField] GameObject pistolBullet;
    [SerializeField] GameObject MGBullet;
    [SerializeField] GameObject sniperBullet;

    public void increasePistolDmg()
    {
        pistolBullet.GetComponent<Bullet>().Dmg += 0.1f;
        Debug.Log(pistolBullet.GetComponent<Bullet>().Dmg);
    }
    public void increaseMGDmg()
    {
        MGBullet.GetComponent<Bullet>().Dmg += 0.1f;

    }
    public void increaseSniperDmg()
    {
        sniperBullet.GetComponent<Bullet>().Dmg += 0.1f;

    }

    public void loadResultScene()
    {
        SceneManager.LoadScene(2);
    }

}
