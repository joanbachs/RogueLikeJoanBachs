﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wepon : MonoBehaviour
{
    public Rigidbody2D bulletPrefab;
    //[SerializeField] private float bulletSpeed;
    [SerializeField] private Transform firePoint;
    private Vector2 dir;

    private AudioSource mAudioSrc;

    void Start()
    {
        mAudioSrc = GetComponent<AudioSource>();
    }

    private void Update()
    {
        dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        dir.Normalize();
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = rotation;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        mAudioSrc.Play();
        Rigidbody2D bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
        //bullet.velocity = dir * bulletSpeed;
    }
}
