﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float dmg;
    [SerializeField] private float _speed;
    public Rigidbody2D rb;
    private Transform _firePoint;
    public bool infiniteBullet;

    private float _mouseX;
    private float _mouseY;
    private Vector3 _initialPos;

    private Vector3 mousePosPix;
    private Vector3 mousePos;

    public Transform FirePoint { get => _firePoint; set => _firePoint = value; }
    public float Dmg { get => dmg; set => dmg = value; }

    private void Awake()
    {
        
    }
    void Start()
    {
        mousePosPix = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePosPix);

        _mouseX = mousePos.x;
        _mouseY = mousePos.y;

        _initialPos = transform.position;

    }

    private void Update()
    {
        float step = _speed * Time.deltaTime;
        if (infiniteBullet)
        {
            rb.AddForce((mousePos - _initialPos) * step, ForceMode2D.Impulse);
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, mousePos, step*10);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Deadzone") || collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("Object"))
        {
            Destroy(gameObject);
        }
    }
}
