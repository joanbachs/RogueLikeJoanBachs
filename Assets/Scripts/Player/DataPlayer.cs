﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DataPlayer : MonoBehaviour
{
    private int _maxHealth = 100;
    private int _currentHp = 100;

    private bool _invulnerabilityActive = false;

    public int MaxHealth { get => _maxHealth; set => _maxHealth = value; }
    public int CurrentHp { get => _currentHp; set => _currentHp = value; }

    private void Start()
    {
        CurrentHp = 100;
        MaxHealth = 100;
    }
    public IEnumerator InvulnerabilityDuration()
    {
        _invulnerabilityActive = true;
        yield return new WaitForSeconds(0.25f);
        _invulnerabilityActive = false;
    }
    public void UpdateMaxHealth(int h)
    {
        CurrentHp += h;
        MaxHealth += h;
        UI.health += h;
    }

    public void UpdateHealth(int h)
    {
        if (CurrentHp + h <= MaxHealth)
        {
            CurrentHp += h;
            UI.health += h;
        }
    }

    private void Update()
    {
        if (CurrentHp <= 0)
        {
            Die();
        }
    }

    public void SetDamage(int dmg)
    {
        _currentHp -= dmg;
        UI.health -= dmg;
    }

    private void Die()
    {
        Destroy(gameObject);
        GameManager.Instance.loadResultScene();
    }
}
