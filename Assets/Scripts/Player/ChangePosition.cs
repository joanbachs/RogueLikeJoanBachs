﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangePosition : MonoBehaviour
{
    void Start()
    {
        RoomManager.OnRoomChange += Repositionate;
    }

    public void Repositionate()
    {
        gameObject.transform.position = new Vector2(0, -2.5f);
    }
}
