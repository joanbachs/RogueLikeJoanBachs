﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public DataPlayer dp;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;
    private float speed;

    private float horizontal;
    private float vertical;

    public Transform playerTransform;

    private Vector2 direction = Vector2.right;
    public float DashForce;
    public float StartDashTimer;
    public float DashCooldown;
    private bool DashReady;
    private bool isDashing;
    float CurrentDashTimer;

    private float timerDashCooldown = 5f;
    public bool canBeDamaged;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        DashReady = true;
        DashCooldown = timerDashCooldown;
        canBeDamaged = true;
    }

    private void Start()
    {
        speed = GameManager.Instance.PlayerSpeed;

    }
    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        Move();
        if (Input.GetKeyDown(KeyCode.LeftShift) && DashReady)
        {
            isDashing = true;
            CurrentDashTimer = StartDashTimer;
            rb.velocity = Vector2.zero;
            DashReady = false;
        }

        if (isDashing)
        {
            canBeDamaged = false;
            CurrentDashTimer -= Time.deltaTime;
            rb.AddForce(direction.normalized * DashForce, ForceMode2D.Impulse);
            if (CurrentDashTimer <= 0)
            {
                isDashing = false;
                rb.velocity = Vector2.zero;
            }
            DashCooldown = timerDashCooldown;
        }
        else
        {
            canBeDamaged = true;
        }

        if (DashCooldown <= 0)
        {
            DashReady = true;
        }
        else
        {
            DashCooldown -= Time.deltaTime;
        }      
    }

    void Move()
    {
        if (horizontal != 0 || vertical != 0)
        {
            direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            transform.Translate(direction*Time.deltaTime*speed);
            //rb.AddForce(direction * speed, ForceMode2D.Force);
        }
        if (horizontal == 0 && vertical == 0)
        {
            rb.velocity = Vector2.zero;
        }
    }
}
