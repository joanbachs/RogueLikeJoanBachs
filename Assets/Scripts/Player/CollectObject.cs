﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectObject : MonoBehaviour
{
    private DataPlayer dp;

    private void Start()
    {
        dp = GetComponent<DataPlayer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case "ExtraHealth":
                dp.UpdateHealth(20);
                Destroy(collision.gameObject);
                break;

            case "MaxHealth":
                dp.UpdateMaxHealth(20);
                Destroy(collision.gameObject);
                break;

            default:
                break;
        }
    }
}
