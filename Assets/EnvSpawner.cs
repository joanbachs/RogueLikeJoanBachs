﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvSpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject[] obj = new GameObject[2];

    float randY;
    float randX;
    int randomObject;
    Vector2 whereToSpawn;

    public bool allObjSpawned = false;
    [SerializeField] private int totalObj;
    public int objSpawned;

    private static EnvSpawner _instance = null;

    public static EnvSpawner Instance
    {
        get { return _instance; }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        objSpawned = 0;
        allObjSpawned = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!allObjSpawned)
        {
            randY = Random.Range(-5f, 4f);
            randX = Random.Range(-15f, 14f);
            if ((randX < -8.77 || randX > -14.7) && (randY < -2.7 || randY > -6.25))
            {
                randX = Random.Range(-15f, 14f);
                randY = Random.Range(-5.4f, 4.2f);
            }
            randomObject = (int)Random.Range(0, 2);
            whereToSpawn = new Vector2(randX, randY);
            GameObject child = Instantiate(obj[randomObject], whereToSpawn, Quaternion.identity) as GameObject;
            child.transform.parent = gameObject.transform;
            objSpawned++;
        }

        if (objSpawned >= totalObj)
        {
            allObjSpawned = true;
        }
    }
}
